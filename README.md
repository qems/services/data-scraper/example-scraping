# example-scraping

We have attached a `docker compose` configuration to run the (for now only) weather data scraper
together with its required services, saving weather forecasts and current weather data from OWM and DWD
to an InfluxDB in a format compatible with other QEMS services and simulators.

The provided configuration instantiates the weather service which grabs the data and sends it out
via a message broker, a MongoDB in which the weather services stores its general state,
RabbitMQ as a AMQP message broker, an InfluxDB to save the data to, and Telegraf to grab the data
from the RabbitMQ Exchange and save it to the InfluxDB.

The services can be started via `docker compose up -d`.

Latitudes and Longitudes can be configured via the Environment Variables `LONGITUDES` and `LATITUDES`
of the weather service in the `docker-compose.yml`. The two lists are pairs where equal position in list means
pairing.

To fetch data from OWM, an API Key is required, which needs to be set in the `OWM_APPID` variable.
The telegraf config is in the `telegraf.conf` file and can be extended for your needs.
